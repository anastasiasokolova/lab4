#include "./linked_list.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

// создать список
struct LinkedList* list_create(int64_t value) {
    struct LinkedList* head = NULL;
    list_add_front(&head, value);
    return head;
}


// добавить элемент в начало списка
void list_add_front(struct LinkedList** head, int64_t value) {
    struct LinkedList* tmp = (struct LinkedList*)malloc(sizeof(struct LinkedList));
    if (tmp) {
        tmp->value = value;
        tmp->next = (*head);
    }
    (*head) = tmp;
}

// добавить элемент в конец списка
void list_add_back(struct LinkedList** head, int64_t value) {
    if (*head == NULL) {
        list_add_front(head, value);
        return;
    }
    struct LinkedList* tmp = (*head);
    while (tmp->next != NULL) {
        tmp = tmp->next;
    }
    struct LinkedList* newNode = (struct LinkedList*)malloc(sizeof(struct LinkedList));
    if (newNode) {
        newNode->next = NULL;
        newNode->value = value;
    }
    tmp->next = newNode;
}

// вернуть элемент по индексу или none_int64 если индекс слишком большой
int64_t list_get(struct LinkedList* head, size_t index) {
    if ((head = list_node_at(head, index)) == NULL)
        return 0;
    return head->value;
}

// Освободить память под весь список
void list_free(struct LinkedList* head) {
    if (head == NULL)
        return;
    list_free(head->next);
    free(head);
}

// Посчитать длину списка
size_t list_length(struct LinkedList* head) {
    size_t length = 0;
    while (head != NULL) {
        length++;
        head = head->next;
    }
    return length;
}

// указатель на структуру по индексу или NULL
struct LinkedList* list_node_at(struct LinkedList* head, size_t index) {
    unsigned int i;
    for (i = index; i > 0; i--) {
        if (head == NULL)
            return NULL;
        head = head->next;
    }
    return head;
}

// Сумма элементов в списке
int64_t list_sum(struct LinkedList* head) {
    int64_t sum = 0;
    while (head != NULL) {
        sum += head->value;
        head = head->next;
    }
    return sum;
}



