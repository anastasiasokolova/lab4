#include "linked_list.h"
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

//считывать элементы пока возможно (пока не дошли до конца потока)
//добавлять их в начало списка
struct LinkedList* initial(void) {
    size_t list_size;
    size_t i;
    int64_t num;
    struct LinkedList* list = NULL;

    puts("Write number of elements:");
    scanf("%zu", &list_size);
    puts("Write numbers:");
    for (i = 0; i < list_size; i++) {
        scanf("%ld", &num);
        list_add_front(&list, num);
    }
    return list;
}

int main(void) {
    struct LinkedList* list = initial();
    size_t length = list_length(list);
    printf("Linked list was created %" PRId64 "\n", length);
    int64_t group_num = 14;

    // вывести сумму списка  
    printf("Sum of the list: %" PRId64 "\n", list_sum(list));

    // вывести элемент списка по индексу, соответствующему вашему номеру в группе. Если список слишком короткий, 
    // то вывести сообщение об ошибке.
    if (group_num < length) {
        printf("Value of the %zu element is: %" PRId64 "\n", group_num, list_get(list, group_num));
     }
    else {
       puts("Index is out of list!");
    }

    list_free(list);
    return 0;
}
