GCC = gcc
GCC_FLAGS = -std=c18 -pedantic -Wall -Werror
LINKED_LIST = linked_list
MAIN = main

all: build

linked_list_obj: $(LINKED_LIST).c
	$(GCC) $(GCC_FLAGS) -c $(LINKED_LIST).c

main_obj: $(MAIN).c
	$(GCC) $(GCC_FLAGS) -c $(MAIN).c


build: linked_list_obj main_obj
	$(GCC) $(GCC_FLAGS) $(MAIN).o $(LINKED_LIST).o -o $(MAIN)
